<?php namespace iBrand\UEditor\Uploader;

use OSS\OssClient;

/**
 *
 *
 * @package iBrand\UEditor\Uploader
 */
trait UploadOss
{
    /**
     * 获取文件路径
     * @return string
     */

    public function uploadOss($key, $content)
    {   
        $path = config('UEditorUpload.core.oss.prefix');
        $accessKeyId = config('UEditorUpload.core.oss.access_id');
        $accessKeySecret = config('UEditorUpload.core.oss.access_key');
        $endpoint = config('UEditorUpload.core.oss.endpoint');
        $bucket = config('UEditorUpload.core.oss.bucket');


        $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
        $response = $ossClient->putObject($bucket,$key,$content);

        $this->fullName=$response['oss-request-url'];
        $this->stateInfo = $this->stateMap[0];
        return true;
    }
}